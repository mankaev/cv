GSFLAGS=-q -dSAFER -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sstdout=%stderr -dPDFSETTINGS=/ebook -dCompatibilityLevel=1.4 -dUseFlateCompression=true -dPermissions=2244 -dEncryptionR=3 -dKeyLength=128 -sOwnerPassword=Andromeda_84 -dFitPages=true

all: Ilia_Mankaev_CV.pdf

Ilia_Mankaev_CV.pdf: Ilia_Mankaev_CV.ps pdfmarks
	@gs ${GSFLAGS} -sOutputFile=$@ -f $< -f pdfmarks

Ilia_Mankaev_CV.ps: cv.1 Makefile
	@groff -Tps -P-pa4 -mpdfmark -mom $< > $@

clean:
	@${RM} Ilia_Mankaev_CV.pdf Ilia_Mankaev_CV.ps
